#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Compile word quiz for cryptoarithmetic."""

__author__ = 'Hwasung Lee'


# -----------------
# User Instructions
#
# Write a function, compile_word(word), that compiles a word of UPPERCASE
# letters as numeric digits. For example:
#   compile_word('YOU') => '(1*U + 10*O +100*Y)'
# Non-uppercase words should remain unchaged.


def compile_word(word):
  """Compile a word of uppercase letters as numeric digits.

  E.g., compile_word('YOU') => '(1*U+10*O+100*Y)'
  Non-uppercase words unchanged: compile_word('+') => '+'
  """
  if word.isupper():
      l = ['10**%s * %s' % (i, w) for (i, w) in enumerate(word[::-1])]
      return '(%s)' % ' + '.join(l)
  else:
      return word

def main():
  """main function which runs when the script is called standalone."""
  pass


if __name__ == '__main__':
  main()

