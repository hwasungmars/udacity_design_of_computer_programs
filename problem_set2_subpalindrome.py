#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Problem set 2: Find the longest subpalindrome."""

__author__ = 'Hwasung Lee'


# --------------
# User Instructions
#
# Write a function, longest_subpalindrome_slice(text) that takes
# a string as input and returns the i and j indices that
# correspond to the beginning and end indices of the longest
# palindrome in the string.
#
# Grading Notes:
#
# You will only be marked correct if your function runs
# efficiently enough. We will be measuring efficency by counting
# the number of times you access each string. That count must be
# below a certain threshold to be marked correct.
#
# Please do not use regular expressions to solve this quiz!


def Maxes(array, key=lambda x: x):
  """Return max elements as an array."""
  # Initialization
  max_so_far = key(array[0])
  result = [array[0]]

  # Iteration
  for item in array[1:]:
    current = key(item)
    if current > max_so_far:
      max_so_far = current
      result = [item]
    elif current == max_so_far:
      result.append(item)
    else:
      pass

  return result


def IsPalendrome(text):
  """Is given text a palendrome."""
  for i in range(len(text)):
    if text[i] != text[-1-i]:
      return False

  return True


def FindLargestSubpalindromeEnding(text):
  """Return the largest subpalindrome at the ending."""
  candidates = [(i, len(text))
      for i in range(len(text)) if IsPalendrome(text[i:len(text)])]
  return max(candidates, key=lambda x: x[1]-x[0])


def LongestSubpalindromes(text):
  """Returns a list of the longest subpalindromes."""
  if len(text) == 0:
    return [(0, 0)]
  elif len(text) == 1:
    return [(0, 1)]
  else:
    interval = FindLargestSubpalindromeEnding(text)
    candidates = LongestSubpalindromes(text[:-1]) + [interval]
    return Maxes(candidates, key=lambda x: x[1]-x[0])


def longest_subpalindrome_slice(text):
  "Return (i, j) such that text[i:j] is the longest palindrome in text."
  text = text.lower()
  return LongestSubpalindromes(text)[0]


def test():
  L = longest_subpalindrome_slice
  assert L('e') == (0, 1)
  assert L('cec') == (0, 3)
  assert L('racecar') == (0, 7)
  assert L('Racecar') == (0, 7)
  assert L('RacecarX') == (0, 7)
  assert L('Race carr') == (7, 9)
  assert L('') == (0, 0)
  assert L('something rac e car going') == (8,21)
  assert L('xxxxx') == (0, 5)
  assert L('Mad am I ma dam.') == (0, 15)
  return 'tests pass'


def main():
  """main function which runs when the script is called standalone."""
  print(test())


if __name__ == '__main__':
  main()

