#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Implementing Udacity's poker helper function kind."""

__author__ = 'Hwasung Lee'


def kind(n, ranks):
  """Return the first rank that this hand has exactly n of.

  Return None if there is no n-of-a-kind in the hand.
  """
  for r in ranks:
    if ranks.count(r) == n:
      return r

  return None


def main():
  """main function which runs when the script is called standalone."""
  assert kind(4, [9, 9, 9, 9, 7]) == 9
  assert kind(1, [9, 9, 9, 9, 7]) == 7


if __name__ == '__main__':
  main()

