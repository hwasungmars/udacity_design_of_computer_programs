#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Brute force programme to find the solution to the red Englishman question."""

__author__ = 'Peter Norvig'


import itertools


def imright(h1, h2):
  """House h1 is immediately to the right of h2 if h1-h2 == 1."""
  return h1-h2 == 1


def nextto(h1, h2):
  """Two houses are next to each other if they differ by 1."""
  return abs(h1-h2) == 1


def instrument_fn(fn, *args):
  c.starts, c.items = 0, 0
  result = fn(*args)
  print('%s got %s with %5d iters over %7d items' %
        (fn.__name__, result, c.starts, c.items))


def c(items):
  """Counter wrapper for a iterable."""
  c.starts += 1
  for item in items:
    c.items += 1
    yield item


def zebra_puzzle():
  """Return a tuple (WATER, ZEBRA) indicating their house numbers."""
  houses = first, _, middle, _, _ = [1, 2, 3, 4, 5]
  orderings = list(itertools.permutations(houses))
  return next((WATER, ZEBRA)
      for (red, green, ivory, yellow, blue) in orderings
      if imright(green, ivory)
      for (Englishman, Spaniard, Ukranian, Japanese, Norwegian) in c(orderings)
      if Englishman == red
      if Norwegian == first
      if Norwegian == blue
      for (dog, snails, fox, horse, ZEBRA) in c(orderings)
      if Spaniard == dog
      for (coffee, tea, milk, oj, WATER) in c(orderings)
      if coffee == green
      if Ukranian == tea
      if milk == middle
      for (OldGold, Kools, Chesterfields, LuckyStrike, Parliaments) in c(orderings)
      if OldGold == snails
      if Kools == yellow
      if nextto(Chesterfields, fox)
      if nextto(Kools, horse)
      if LuckyStrike == oj
      if Japanese == Parliaments
  )


def main():
  """main function which runs when the script is called standalone."""
  instrument_fn(zebra_puzzle,)


if __name__ == '__main__':
  main()

