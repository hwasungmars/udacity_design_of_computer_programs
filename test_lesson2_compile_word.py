#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Tests for compile word."""

__author__ = 'Hwasung Lee'


import unittest
import lesson2_compile_word


class TestCompileWord(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testCompileWord(self):
    """Describe this test."""
    word = 'YOU'
    expected = '(10**0 * U + 10**1 * O + 10**2 * Y)'
    result = lesson2_compile_word.compile_word(word)
    self.assertEqual(expected, result)

    word = '+'
    expected = '+'
    result = lesson2_compile_word.compile_word(word)
    self.assertEqual(expected, result)

    word = 'lower'
    expected = 'lower'
    result = lesson2_compile_word.compile_word(word)
    self.assertEqual(expected, result)




if __name__ == '__main__':
  unittest.main()

