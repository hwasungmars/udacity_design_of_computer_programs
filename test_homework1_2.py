#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Tests for homework 1-2."""

__author__ = 'Hwasung Lee'


import unittest
import homework1_2


class TestJokerPocker(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testExpandCard(self):
    """Test whether joker expansion works."""
    card = '?B'
    expected = set([
        '2C', '3C', '4C', '5C', '6C', '7C', '8C', '9C', 'TC', 'JC', 'QC', 'KC',
        'AC', '2S', '3S', '4S', '5S', '6S', '7S', '8S', '9S', 'TS', 'JS', 'QS',
        'KS', 'AS'])
    result = set(homework1_2.ExpandCard(card))
    self.assertEqual(expected, result)

    card = '?R'
    expected = set([
        '2H', '3H', '4H', '5H', '6H', '7H', '8H', '9H', 'TH', 'JH', 'QH', 'KH',
        'AH', '2D', '3D', '4D', '5D', '6D', '7D', '8D', '9D', 'TD', 'JD', 'QD',
        'KD', 'AD'])
    result = set(homework1_2.ExpandCard(card))
    self.assertEqual(expected, result)

  def testBestHand(self):
    """Test whether BestHand returns the correct candidate."""
    hand = '6C 7C 8C 9C TC 5C JC'.split()
    expected = set(['7C', '8C', '9C', 'JC', 'TC'])
    result = set(homework1_2.BestHand(hand))
    self.assertEqual(expected, result)

    hand = 'TD TC 5H 5C 7C TH TS'.split()
    expected = set(['7C', 'TC', 'TD', 'TH', 'TS'])
    result = set(homework1_2.BestHand(hand))
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

