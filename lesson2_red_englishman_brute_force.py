#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Brute force programme to find the solution to the red Englishman question."""

__author__ = 'Peter Norvig'


import itertools


def imright(h1, h2):
  """House h1 is immediately to the right of h2 if h1-h2 == 1."""
  return h1-h2 == 1


def nextto(h1, h2):
  """Two houses are next to each other if they differ by 1."""
  return abs(h1-h2) == 1


def zebra_puzzle():
  """Return a tuple (WATER, ZEBRA) indicating their house numbers."""
  houses = first, _, middle, _, _ = [1, 2, 3, 4, 5]
  orderings = list(itertools.permutations(houses)) #1
  return ((WATER, ZEBRA)
      for (red, green, ivory, yellow, blue) in orderings
      for (Englishman, Spaniard, Ukranian, Japanese, Norwegian) in orderings
      for (dog, snails, fox, horse, ZEBRA) in orderings
      for (coffee, tea, milk, oj, WATER) in orderings
      for (OldGold, Kools, Chesterfields, LuckyStrike, Parliaments) in orderings
      if Englishman == red
      if Spaniard == dog
      if coffee == green
      if Ukranian == tea
      if imright(green, ivory)
      if OldGold == snails
      if Kools == yellow
      if milk == middle
      if Norwegian == first
      if nextto(Chesterfields, fox)
      if nextto(Kools, horse)
      if LuckyStrike == oj
      if Japanese == Parliaments
      if Norwegian == blue
  )


def main():
  """main function which runs when the script is called standalone."""
  print('The solution(s) to the red Englishman question is:')
  for w, z in zebra_puzzle():
    print('Water in %s, zebra in %s' % (w, z))




if __name__ == '__main__':
  main()

