#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Udacity problem set 2, floor puzzle"""

__author__ = 'Hwasung Lee'


#------------------
# User Instructions
#
# Hopper, Kay, Liskov, Perlis, and Ritchie live on
# different floors of a five-floor apartment building.
#
# Hopper does not live on the top floor.
# Kay does not live on the bottom floor.
# Liskov does not live on either the top or the bottom floor.
# Perlis lives on a higher floor than does Kay.
# Ritchie does not live on a floor adjacent to Liskov's.
# Liskov does not live on a floor adjacent to Kay's.
#
# Where does everyone live?
#
# Write a function floor_puzzle() that returns a list of
# five floor numbers denoting the floor of Hopper, Kay,
# Liskov, Perlis, and Ritchie.

import itertools


def AreAdjacent(a, b):
  """Are two floors adjacent."""
  return abs(a - b) == 1


def IsHigherFloor(a, b):
  """Is a higher than b."""
  return a - b > 0


def floor_puzzle():
  """Solves the floor_puzzle."""
  floors = bottom_floor, _, _, _, top_floor = [1, 2, 3, 4, 5]
  return next([Hopper, Kay, Liskov, Perlis, Ritchie]
      for Hopper, Kay, Liskov, Perlis, Ritchie in itertools.permutations(floors)
      if Hopper != top_floor
      if Kay != bottom_floor
      if Liskov != top_floor and Liskov != bottom_floor
      if IsHigherFloor(Perlis, Kay)
      if not AreAdjacent(Ritchie, Liskov)
      if not AreAdjacent(Liskov, Kay))


def main():
  """main function which runs when the script is called standalone."""
  # Surprisingly the answer is unique.
  print(floor_puzzle())


if __name__ == '__main__':
  main()

