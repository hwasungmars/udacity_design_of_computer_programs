#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Unit tests for Udacity subpalindrome problem."""

__author__ = 'Hwasung Lee'


import unittest

import problem_set2_subpalindrome


class TestSubpalindrome(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testMaxes(self):
    """Returns a array of max elements."""
    array = [-1, 0, 1]
    squares = lambda x: x*x
    expected = [-1, 1]
    result = problem_set2_subpalindrome.Maxes(array, key=squares)
    self.assertEqual(expected, result)

  def testPalendrome(self):
    """Tests whether a given string is a palendrome."""
    text = ''
    self.assertTrue(problem_set2_subpalindrome.IsPalendrome(text))

    text = 'a'
    self.assertTrue(problem_set2_subpalindrome.IsPalendrome(text))

    text = 'ab'
    self.assertFalse(problem_set2_subpalindrome.IsPalendrome(text))

    text = 'aba'
    self.assertTrue(problem_set2_subpalindrome.IsPalendrome(text))

  def testFindLargestSubpalindromeEnding(self):
    """Find the largest subpalindrome ending."""
    text = 'ab'
    expected = (1, 2)
    result = problem_set2_subpalindrome.FindLargestSubpalindromeEnding(text)
    self.assertEqual(expected, result)

    text = 'racecar'
    expected = (0, len(text))
    result = problem_set2_subpalindrome.FindLargestSubpalindromeEnding(text)
    self.assertEqual(expected, result)

    text = 'rarar'
    expected = (0, len(text))
    result = problem_set2_subpalindrome.FindLargestSubpalindromeEnding(text)
    self.assertEqual(expected, result)

  def testLongestSubpalindromes(self):
    """Find the longest subpalidromes."""
    text = 'a'
    expected = [(0, len(text))]
    result = problem_set2_subpalindrome.LongestSubpalindromes(text)
    self.assertEqual(expected, result)

    text = 'ab'
    expected = [(0, 1), (1, 2)]
    result = problem_set2_subpalindrome.LongestSubpalindromes(text)
    self.assertEqual(expected, result)

    text = 'racecar'
    expected = [(0, len(text))]
    result = problem_set2_subpalindrome.LongestSubpalindromes(text)
    self.assertEqual(expected, result)

    text = 'abaca'
    expected = [(0, 3), (2, 5)]
    result = problem_set2_subpalindrome.LongestSubpalindromes(text)
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

